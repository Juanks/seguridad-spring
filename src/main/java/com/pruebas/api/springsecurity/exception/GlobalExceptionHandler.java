package com.pruebas.api.springsecurity.exception;

import java.time.LocalDateTime;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.pruebas.api.springsecurity.dto.ApiError;

import jakarta.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class GlobalExceptionHandler {
    
    @ExceptionHandler(Exception.class) //tambien se puede configurar con json
    public ResponseEntity<?> handlerGenericException(HttpServletRequest request, Exception exception){//este metodo captura error de database, null pointer, genericos
        ApiError apiError = new ApiError();
        apiError.setBackendMessage(exception.getLocalizedMessage());
        apiError.setUrl(request.getRequestURL().toString());
        apiError.setMethod(request.getMethod());
        apiError.setMessage("Error interno del servidor!!");

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(apiError);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class) //se lanza cuando no logra realizar binding de json al objeto java
    public ResponseEntity<?> handlerMethodArgumentNotValidException(HttpServletRequest request, 
                                                                    MethodArgumentNotValidException exception){
        ApiError apiError = new ApiError();
        apiError.setBackendMessage(exception.getLocalizedMessage());
        apiError.setUrl(request.getRequestURL().toString());
        apiError.setMethod(request.getMethod());
        apiError.setTimestamp(LocalDateTime.now());
        apiError.setMessage("Error en petición enviada!!");

        /*System.out.println("captura el error: "+
                            exception.getAllErrors().stream().map(each -> each.getDefaultMessage()).collect(Collectors.toList())
        );*/

        System.out.println("captura el error: "+exception.getAllErrors().stream().map(each -> each.getDefaultMessage()));

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(apiError);
    }
}
