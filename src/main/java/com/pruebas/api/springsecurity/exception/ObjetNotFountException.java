package com.pruebas.api.springsecurity.exception;

public class ObjetNotFountException extends RuntimeException{
    public ObjetNotFountException(){}

    public ObjetNotFountException(String message){
        super(message);
    }

    public ObjetNotFountException(String messege, Throwable cause){
        super(messege, cause);
    }
}
