package com.pruebas.api.springsecurity.config.security;

import com.pruebas.api.springsecurity.exception.ObjetNotFountException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.pruebas.api.springsecurity.persistence.repository.UserRepository;

@Configuration
public class SecurityBeansInjector {

    @Autowired
    private UserRepository userRepository;

   /* @Autowired
    private AuthenticationConfiguration authenticationConfiguration;
    */

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration) throws Exception{
        return authenticationConfiguration.getAuthenticationManager();        
    }

    //se implementa la estrategia de autenticacion
    @Bean
    public AuthenticationProvider authenticationProvider(){
        DaoAuthenticationProvider authenticationStategy = new DaoAuthenticationProvider();
       /* authenticationStategy.setPasswordEncoder(null);
        authenticationStategy.setUserDetailsService(null);*/

        authenticationStategy.setPasswordEncoder(passwordEncoder());
        authenticationStategy.setUserDetailsService(userDetailsService());

        return authenticationStategy;
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    /*@Bean
    public UserDetailsService userDetailsService(){
        return new UserDetailsService() {

            @Override
            public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
                // TODO Auto-generated method stub
                throw new UnsupportedOperationException("Unimplemented method 'loadUserByUsername'");
            }
            
        };
    }*/


    @Bean
    public UserDetailsService userDetailsService(){
        return (username) -> {
            return userRepository.findByUsername(username)
                    .orElseThrow(() -> new ObjetNotFountException("User not found with username "+username));
        };
    }
}
