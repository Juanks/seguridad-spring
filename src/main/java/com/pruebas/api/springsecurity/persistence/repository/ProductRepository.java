package com.pruebas.api.springsecurity.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pruebas.api.springsecurity.persistence.entity.Product;

public interface ProductRepository extends JpaRepository<Product, Long>{

}
