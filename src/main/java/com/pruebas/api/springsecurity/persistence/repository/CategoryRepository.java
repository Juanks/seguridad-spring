package com.pruebas.api.springsecurity.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pruebas.api.springsecurity.persistence.entity.Category;

public interface CategoryRepository extends JpaRepository<Category, Long>{
    
}
