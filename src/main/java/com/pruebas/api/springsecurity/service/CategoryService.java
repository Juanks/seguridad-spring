package com.pruebas.api.springsecurity.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.pruebas.api.springsecurity.dto.SaveCategory;
import com.pruebas.api.springsecurity.persistence.entity.Category;

import jakarta.validation.Valid;

public interface CategoryService {

    Page<Category> findAll(Pageable pageable);

    Optional<Category> findOneById(Long categoryId);

    Category createOne(@Valid SaveCategory saveCategory);

    Category updateOneById(Long categoryId, @Valid SaveCategory saveCategory);

    Category disableOneById(Long categorytId);
    
}
