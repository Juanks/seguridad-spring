package com.pruebas.api.springsecurity.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.pruebas.api.springsecurity.dto.SaveProduct;
import com.pruebas.api.springsecurity.exception.ObjetNotFountException;
import com.pruebas.api.springsecurity.persistence.entity.Category;
import com.pruebas.api.springsecurity.persistence.entity.Product;
import com.pruebas.api.springsecurity.persistence.repository.ProductRepository;
import com.pruebas.api.springsecurity.service.ProductService;

import jakarta.validation.Valid;

@Service
public class ProductServiceImpl implements ProductService{

    @Autowired
    private ProductRepository productRepository;

    @Override
    public Page<Product> findAll(Pageable pageable) {
        
        return productRepository.findAll(pageable);
    }

    @Override
    public Optional<Product> findOneById(Long productId) {
        return productRepository.findById(productId);
    }

    @Override
    public Product createOne(@Valid SaveProduct saveProduct) {
        Product product = new Product();
        product.setName(saveProduct.getName());
        product.setPrice(saveProduct.getPrice());
        product.setStatus(Product.ProductStatus.ENABLED);

        Category category = new Category();
        category.setId(saveProduct.getCategoryId());
        product.setCategory(category);

        return productRepository.save(product);
    }

    @Override
    public Product updateOneById(Long productId, @Valid SaveProduct saveProduct) {
        Product productFromDB = productRepository.findById(productId)
                                .orElseThrow( () -> new ObjetNotFountException("Product not found with id "+productId) );

        productFromDB.setName(saveProduct.getName());
        productFromDB.setPrice(saveProduct.getPrice());

        Category category = new Category();
        category.setId(saveProduct.getCategoryId());
        productFromDB.setCategory(category);

        return productRepository.save(productFromDB);
    }

    @Override
    public Product disableOneById(Long productId) {
        Product productFromDB = productRepository.findById(productId)
                                .orElseThrow( () -> new ObjetNotFountException("Product not found with id "+productId) );

        productFromDB.setStatus(Product.ProductStatus.DISABLED);

        return productRepository.save(productFromDB);                                
    }

    @Override
    public Product enableOneById(Long productId) {
        Product productFromDB = productRepository.findById(productId)
                                .orElseThrow( () -> new ObjetNotFountException("Product not found with id "+productId) );

        productFromDB.setStatus(Product.ProductStatus.ENABLED);

        return productRepository.save(productFromDB);   
    }
    
}
