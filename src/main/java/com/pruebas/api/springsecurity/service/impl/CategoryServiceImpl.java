package com.pruebas.api.springsecurity.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.pruebas.api.springsecurity.dto.SaveCategory;
import com.pruebas.api.springsecurity.dto.SaveProduct;
import com.pruebas.api.springsecurity.exception.ObjetNotFountException;
import com.pruebas.api.springsecurity.persistence.entity.Category;
import com.pruebas.api.springsecurity.persistence.entity.Product;
import com.pruebas.api.springsecurity.persistence.repository.CategoryRepository;
import com.pruebas.api.springsecurity.persistence.repository.ProductRepository;
import com.pruebas.api.springsecurity.service.CategoryService;
import com.pruebas.api.springsecurity.service.ProductService;

import jakarta.validation.Valid;

@Service
public class CategoryServiceImpl implements CategoryService{
    
    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public Page<Category> findAll(Pageable pageable) {
        
        return categoryRepository.findAll(pageable);
    }

    @Override
    public Optional<Category> findOneById(Long categoryId) {
        return categoryRepository.findById(categoryId);
    }

    @Override
    public Category createOne(@Valid SaveCategory saveCategory) {
        Category category = new Category();
        category.setName(saveCategory.getName());
        category.setStatus(Category.CategoryStatus.ENABLED);

        return categoryRepository.save(category);
    }

    @Override
    public Category updateOneById(Long categoryId, @Valid SaveCategory saveCategory) {
        Category categoryFromDB = categoryRepository.findById(categoryId)
                                .orElseThrow( () -> new ObjetNotFountException("Product not found with id "+categoryId) );

        categoryFromDB.setName(saveCategory.getName());        

        return categoryRepository.save(categoryFromDB);
    }

    @Override
    public Category disableOneById(Long categoryId) {
        Category categoryFromDB = categoryRepository.findById(categoryId)
                                .orElseThrow( () -> new ObjetNotFountException("Product not found with id "+categoryId) );

        categoryFromDB.setStatus(Category.CategoryStatus.DISABLED);

        return categoryRepository.save(categoryFromDB);                                
    }
    
}
